const colors = require('tailwindcss/colors')

module.exports = {
  purge: [
    './components/**/*.tsx',
    './components/**/**/*.tsx',
    './components/**/**/**/*.tsx',
    './pages/**/*.tsx',
    './pages/*.tsx',
    './layouts/**/*.tsx'
  ],

  darkMode: false, // or 'media' or 'class'
  theme: {
    backgroundColor: theme => ({
      ...theme('colors'),
      graybg: '#F8F8F8',
    }),
    fontFamily: {
      body: ['"Open Sans"'],
      quote:['Neuton'],
      sans: ['Montserrat', 'Lato', 'sans-serif'],
      serif: ['Montserrat', 'serif']
    },
    extend: {
      colors: {
        transparent: 'transparent',
      },
      minHeight: {
        40: '10rem'
      },
      zIndex: {
        99: 99,
        999: 999
      },
      height: {
        136: '34rem',
        '853px': '853px'
      },
      maxHeight: {
        menu: "650px"
      },
      boxShadow: {
        '3xl': '10px 10px 10px rgba(0, 0, 0, 0.3)',
        '4xl': '10px 10px 10px rgba(0, 0, 0, 1)',
      }
    }
  },
  variants: {
    extend: {
      display: ['group-hover'],
      visibility: ['group-hover'],
      padding: ['hover'],
    }
  },
  plugins: []
}
