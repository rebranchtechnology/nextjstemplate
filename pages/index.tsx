import BaseLayout from '../layouts/Base'
import { pageRoutes } from '../lib/routes'

export default function Home() {
  return (
    <BaseLayout
      title='Home'
      selectedNavKey='home'
      description='Home'
      canonical={pageRoutes.home}
    >
    </BaseLayout>
  )
}
