import Image from 'next/image'

import { pageRoutes } from '../../../lib/routes'
import { navKeys } from '../../../lib/types'

export default function Footer(props: { selectedNavKey: navKeys }) {
  const quickLinks = [
    {
      label: 'Home',
      link: pageRoutes.home
    }
  ]

  return (
    <footer className='bg-white m-auto' aria-labelledby='footer-heading'>
      {props.selectedNavKey === 'home' && <div className='w-3/4 py-20 mx-auto'></div>}
      <div className='w-full border-t-2 bg-gray-100'>
        <div className='w-3/4 mx-auto py-10 flex flex-wrap justify-between'>
          <div className='w-full sm:w-1/4 mt-3'>
            <h3>Quick Links</h3>
            <ul className='mt-8'>
              {quickLinks.map(ql => (
                <li key={ql.link}>
                  <a className='hover:text-blue-800' href={ql.link}>
                    {ql.label}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
      <div className='w-full bg-black text-white text-center py-3'>
        <p>© SomeCompany</p>
      </div>
    </footer>
  )
}
