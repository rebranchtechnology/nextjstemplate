import React, { Fragment, ReactNode, useState } from 'react'

import Image from 'next/image'

import { pageRoutes } from '../../../lib/routes'
import { headerText } from '../../../lib/constants/headers'
import { navKeys } from '../../../lib/types'

const navLinks: { href: string; label: string; navKey: navKeys; subLinks?: any[] }[] = [
  {
    href: pageRoutes.home,
    label: 'HOME',
    navKey: 'home'
  }
]

export default function HeaderTW(props: { selectedNavKey: navKeys }) {
  const DesktopHref = (p: { name: string; href: string; navKey: navKeys }) => {
    return <></>
  }

  const DesktopListPop = (p: { name: string; list: any[]; children?: ReactNode; navKey: navKeys }) => {
    return <></>
  }

  const NavAccordion = (p: { href: string; label: string; navKey: string; subLinks: any[] }) => {
    const { label, href, navKey, subLinks } = p
    return <></>
  }

  return <></>
}
